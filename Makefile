CC  = gcc
ASM = nasm

INPUT_FILENAME  = in.bmp
OUTPUT_FILENAME = out.bmp

BIN_NAME = sepia.elf

SRC_DIR = src

CFLAGS = -I$(SRC_DIR)

C_SRCS   := $(shell find $(SRC_DIR) -name '*.c')
ASM_SRCS := $(shell find $(SRC_DIR) -name '*.asm')

OBJS += $(patsubst %.c,%.o,$(C_SRCS))
OBJS += $(patsubst %.asm,%.o,$(ASM_SRCS))

.PHONY: all
all: compare clean

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

%.o: %.asm
	$(ASM) -felf64 -o $@ $<

.PHONY: build
build: $(OBJS)
	$(CC) -o $(BIN_NAME) $^

.PHONY: compare
compare: build
	echo "C implementation:"
	./$(BIN_NAME) $(INPUT_FILENAME) $(OUTPUT_FILENAME) --performance-check
	echo "ASM implementation:"
	./$(BIN_NAME) $(INPUT_FILENAME) $(OUTPUT_FILENAME) --asm --performance-check

.PHONY: clean
clean:
	find . -type f -name '*.o' -delete
	rm $(BIN_NAME)
