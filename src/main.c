#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "msg.h"
#include "args.h"
#include "img/bmp/io.h"
#include "img/bmp/msg.h"
#include "util/util.h"
#include "img/transformation/filter.h"

#define PERFORMANCE_RUN_NUMBER 100

extern void apply_sepia_filter(struct image *image);

static inline void run_performance_test(struct image *image) {
    clock_t begin = clock();
    for (size_t i = 0; i < PERFORMANCE_RUN_NUMBER; i++) {
        apply_sepia_filter(image);
    }
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    
    clock_t single_call_begin = clock();
    apply_sepia_filter(image);
    clock_t single_call_end = clock();
    double single_call_time_spent = (double)(single_call_end - single_call_begin) / CLOCKS_PER_SEC;

    printf("Single function call: %fs\n", single_call_time_spent);
    printf("%d function calls: %fs\n", PERFORMANCE_RUN_NUMBER, time_spent);
}

int main(int argc, char** argv) {
    struct arguments args = { 0 };

    enum argument_parse_status arg_parse_status = parse_arguments(argc, argv, &args);
    if (arg_parse_status != ARG_PARSE_OK) {
        eprint(ARG_PARSE_STATUS_MESSAGES[arg_parse_status]);
        return 1;
    }

    FILE *in = fopen(args.in, "r");
    if (in == NULL) {
        perror("Couldn't open IN file");
        return 1;
    }

    struct image image = {0};

    enum bmp_read_status read_status = from_bmp(in, &image);
    fclose(in);
    if (read_status != BMP_READ_OK) {
        eprint(BMP_READ_STATUS_MESSAGES[read_status]);
        return 1;
    }

    FILE *out = fopen(args.out, "w");
    if (out == NULL) {
        perror("Couldn't open OUT file");
        return 1;
    }

    if (args.use_asm_impl) {
        filter_use_asm_impl();
        puts("asm");
    } else {
        filter_use_default_impl();
        puts("default");
    }

    if (args.performance_check_mode) {
        run_performance_test(&image);
    } else {
        apply_sepia_filter(&image);
    }

    enum bmp_write_status write_status = to_bmp(out, &image);
    fclose(out);
    image_destroy(&image);
    if (write_status != BMP_WRITE_OK) {
        eprint(BMP_WRITE_STATUS_MESSAGES[write_status]);
        return 1;
    }

    return 0;
}
