#pragma once

#include <stdbool.h>
#include <stdint.h>

struct arguments {
    char *in;
    char *out;
    bool use_asm_impl;
    bool performance_check_mode;
};

enum argument_parse_status {
    ARG_PARSE_OK = 0,
    ARG_PARSE_NOT_ENOUGH,
    __ARG_PARSE_STATUS_N
};

enum argument_parse_status parse_arguments(int argc, char* argv[], struct arguments *args);
