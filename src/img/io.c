#include "io.h"

#include <stdint.h>
#include <stdio.h>

size_t get_file_size(FILE *f) {
    const int64_t saved_pos = ftell(f);

    fseek(f, 0, SEEK_END);
    size_t file_size = ftell(f);
    fseek(f, saved_pos, SEEK_SET);

    return file_size;
}
