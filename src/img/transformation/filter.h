#pragma once

#include "img/image.h"

// (globally) Use filter default (C) implementation
void filter_use_default_impl();
// (globally) Use filter ASM implementation
void filter_use_asm_impl();

// Apply sepia filter to the image (replaces the original one)
void apply_sepia_filter(struct image* image);
