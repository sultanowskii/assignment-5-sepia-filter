#include "img/image.h"

static const float BLUE_FACTORS[]  __attribute__((aligned(16))) = {.131, .168, .189};
static const float GREEN_FACTORS[] __attribute__((aligned(16))) = {.534, .686, .769};
static const float RED_FACTORS[]   __attribute__((aligned(16))) = {.272, .349, .393};

extern void apply_sepia_filter_to_pixel_asm_impl(
    struct pixel *pixel,
    const float blue_factors[3],
    const float green_factors[3],
    const float red_factors[3]
);

static inline uint8_t float_to_color_value(float value) {
    if (value < 0) {
        return 0;
    } else if (value > UINT8_MAX) {
        return UINT8_MAX;
    } else {
        return (uint8_t)value;
    }
}

static void apply_sepia_filter_to_pixel_c_impl(
    struct pixel *pixel,
    const float blue_factors[3],
    const float green_factors[3],
    const float red_factors[3]
) {
    uint8_t orig_blue = pixel->b;
    uint8_t orig_green = pixel->g;
    uint8_t orig_red = pixel->r;

    float raw_b = (orig_blue * blue_factors[0]) + (orig_green * green_factors[0]) + (orig_red * red_factors[0]);
    float raw_g = (orig_blue * blue_factors[1]) + (orig_green * green_factors[1]) + (orig_red * red_factors[1]);
    float raw_r = (orig_blue * blue_factors[2]) + (orig_green * green_factors[2]) + (orig_red * red_factors[2]);

    pixel->b = float_to_color_value(raw_b);
    pixel->g = float_to_color_value(raw_g);
    pixel->r = float_to_color_value(raw_r);
}

void (*apply_sepia_filter_to_pixel)(
    struct pixel *pixel,
    const float blue_factors[3],
    const float green_factors[3],
    const float red_factors[3]
);

void filter_use_asm_impl() {
    apply_sepia_filter_to_pixel = apply_sepia_filter_to_pixel_asm_impl;
}

void filter_use_default_impl() {
    apply_sepia_filter_to_pixel = apply_sepia_filter_to_pixel_c_impl;
}

void apply_sepia_filter(struct image* image) {
    for (size_t y = 0; y < image->height; y++) {
        for (size_t x = 0; x < image->width; x++) {
            size_t index = get_pixel_index_at_coords(image, y, x);

            apply_sepia_filter_to_pixel(&(image->data[index]), BLUE_FACTORS, GREEN_FACTORS, RED_FACTORS);
        }
    }
}
