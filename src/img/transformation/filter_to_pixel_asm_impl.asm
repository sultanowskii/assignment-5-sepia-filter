global apply_sepia_filter_to_pixel_asm_impl

%define UINT8_MAX 255

section .text

; Transforms uint8 to float and puts it 4 times into SSE register
; arg1 (operand 1): SSE register
; arg2 (operand 2): source register containing a number
; 
; affects eax, mm7
%macro uint8_to_4_floats 2
    ; eax = int(arg2)
    mov al, %2
    and rax, 0xff

    ; mm7 = rax
    movq mm7, rax

    ; arg2[0] = float(mm7[0])
    cvtpi2ps %1, mm7

    ; arg2[0] = arg2[00b]
    ; arg2[1] = arg2[00b]
    ; arg2[2] = arg2[00b]
    ; arg2[3] = arg2[00b]
    shufps %1, %1, 00000000b
%endmacro

%assign STACK_SIZE 16
; args:
; RDI: struct pixel *
; RSI: blue_factors[3] (16-aligned)
; RDX: green_factors[3] (16-aligned)
; RCX: red_factors[3] (16-aligned)
;
; vars:
; rsp+0: new_b
; rsp+4: new_g
; rsp+8: new_r
apply_sepia_filter_to_pixel_asm_impl:
    sub rsp, STACK_SIZE

    ; xmm0 = B
    uint8_to_4_floats xmm0, byte [rdi + 0]
    
    ; xmm0 *= blue_factors
    mulps xmm0, [rsi]

    ; xmm1 = G
    uint8_to_4_floats xmm1, byte [rdi + 1]

    ; xmm1 *= green_factors
    mulps xmm1, [rdx]

    ; xmm2 = R
    uint8_to_4_floats xmm2, byte [rdi + 2]

    ; xmm2 *= red_factors
    mulps xmm2, [rcx]

    ; result = xmm0 + xmm1 + xmm2
    addps xmm0, xmm1
    addps xmm0, xmm2

    ; xmm0 = max(xmm0, 0)
    pxor xmm1, xmm1
    maxps xmm0, xmm1

    ; xmm0 = min(xmm0, float(255))
    movaps xmm1, [rel UINT8_MAX_VECTOR]
    cvtdq2ps xmm1, xmm1
    minps xmm0, xmm1

    ; transform floats to ints
    cvtps2dq xmm0, xmm0

    ; save vector on stack
    movdqu [rsp + 0], xmm0

    ; assign new b, g, r values
    mov al, byte [rsp]
    mov byte [rdi + 0], al
    mov al, byte [rsp + 4]
    mov byte [rdi + 1], al
    mov al, byte [rsp + 8]
    mov byte [rdi + 2], al

    add rsp, STACK_SIZE
    ret
%undef STACK_SIZE

section .data:
    UINT8_MAX_VECTOR: dd UINT8_MAX, UINT8_MAX, UINT8_MAX, UINT8_MAX
