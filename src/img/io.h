#pragma once

#include <stdio.h>
#include <stdlib.h>

size_t get_file_size(FILE *f);
