#include "img/bmp/io.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "img/bmp/header.h"
#include "img/bmp/util.h"
#include "img/bmp/validation.h"

// reserved and must be 0
#define BF_RESERVED 0
// BITMAPINFO size
#define BI_SIZE 40
// the only allowed value
#define BI_PLANES 1
// byte size in bits
#define BITS_IN_BYTE 8
// 2D array
#define BI_COMPRESSION 0

#define BI_X_PELS_PER_METER 0
#define BI_Y_PELS_PER_METER 0

// no color table is used
#define BI_CLR_USED 0 
#define BI_CLR_IMPORTANT 0

void bmp_fill_image(FILE *in, struct image *img, struct bmp_header *header) {
    const uint64_t height = header->biHeight;
    const uint64_t width = header->biWidth;

    const long saved_file_pos = ftell(in);

    size_t last_row_beginning_index = (height - 1) * width;
    struct pixel *pixel_ptr = img->data + last_row_beginning_index;
    uint64_t row_size = width * sizeof(struct pixel);
    int64_t padding = (int64_t)bmp_get_padding(row_size);

    for (size_t i = 0; i < height; i++) {
        fread(pixel_ptr, sizeof(struct pixel), width, in);
        fseek(in, padding, SEEK_CUR);
        pixel_ptr -= width;
    }

    fseek(in, saved_file_pos, SEEK_SET);
}

enum bmp_read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = { 0 };

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return BMP_READ_IO_ERROR;
    }

    if (!bmp_is_signature_valid(&header)) {
        return BMP_READ_INVALID_SIGNATURE;
    }

    if (!bmp_is_size_info_valid(&header, in)) {
        return BMP_READ_INVALID_SIZE_INFO;
    }

    bool is_created_successfully;
    *img = image_create(header.biHeight, header.biWidth, &is_created_successfully);
    if (!is_created_successfully) {
        return BMP_READ_MEMORY_ALLOCATION_ERROR;
    }

    bmp_fill_image(in, img, &header);

    return BMP_READ_OK;
}

static struct bmp_header bmp_header_from_image(struct image const *img) {
    const uint32_t height = img->height;
    const uint32_t width = img->width;

    const uint64_t row_size = width * sizeof(struct pixel);
    const uint32_t padded_row_size = bmp_pad_row_size(row_size);
    const uint32_t padded_pixel_data_size = padded_row_size * height;

    return (struct bmp_header) {
        .bfType = BMP_SIGNATURE,
        .bfileSize = sizeof(struct bmp_header) + padded_pixel_data_size,
        .bfReserved = BF_RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BI_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = BI_PLANES,
        .biBitCount = sizeof(struct pixel) * BITS_IN_BYTE,
        .biCompression = BI_COMPRESSION,
        .biSizeImage = padded_pixel_data_size,
        .biXPelsPerMeter = BI_X_PELS_PER_METER,
        .biYPelsPerMeter = BI_Y_PELS_PER_METER,
        .biClrUsed = BI_CLR_USED,
        .biClrImportant = BI_CLR_IMPORTANT,
    };
}

enum bmp_write_status to_bmp(FILE* out, struct image const* img) {
    const char filler[4] = {0};

    const uint32_t height = img->height;
    const uint32_t width = img->width;
    const uint64_t row_size = width * sizeof(struct pixel);

    const struct bmp_header header = bmp_header_from_image(img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return BMP_WRITE_IO_ERROR;
    }

    const size_t last_row_beginning_index = (height - 1) * width;
    struct pixel *pixel_ptr = img->data + last_row_beginning_index;
    const uint64_t padding = bmp_get_padding(row_size);

    for (size_t i = 0; i < height; i++) {
        if (fwrite(pixel_ptr, sizeof(struct pixel), width, out) != width) {
            return BMP_WRITE_IO_ERROR;
        }
        if (!padding || fwrite(filler, padding, 1, out) != 1) {
            return BMP_WRITE_IO_ERROR;
        }
        pixel_ptr -= width;
    }

    return BMP_WRITE_OK;
}
