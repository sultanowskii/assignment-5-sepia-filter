#include "args.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum argument_parse_status parse_arguments(int argc, char* argv[], struct arguments *args) {
    if (argc < 3) {
        return ARG_PARSE_NOT_ENOUGH;
    }

    args->in = argv[1];
    args->out = argv[2];
    args->use_asm_impl = false;
    args->performance_check_mode = false;

    for (int i = 3; i < argc; i++) {
        if (!strcmp(argv[i], "--asm")) {
            args->use_asm_impl = true;
        }
        if (!strcmp(argv[i], "--performance-check")) {
            args->performance_check_mode = true;
        }
    }

    return ARG_PARSE_OK;
}
